<?php

declare(strict_types=1);

namespace app\SearchStrategy;

use app\ArrayModel\SortedUniqueValueArray;

abstract class AbstractSearchStrategy implements SearchStrategyInterface
{
    protected const NEGATIVE_RESULT = -1;
    abstract public function findLowerThenTarget(int $target, SortedUniqueValueArray $numbers): int;

    protected function isPreSearchData(int $target, SortedUniqueValueArray $numbers): bool
    {
        $cnt = count($numbers);
        return $cnt < 2 || $numbers[0] >= $target || $numbers[$cnt-1] < $target;
    }

    protected function getPreSearchData(int $target, SortedUniqueValueArray $numbers): int
    {
        $cnt = count($numbers);
        if ($cnt < 2) {
            return self::NEGATIVE_RESULT;
        }

        if ($numbers[0] >= $target) {
            return self::NEGATIVE_RESULT;
        }

        if ($numbers[$cnt-1] < $target) {
            return $numbers[$cnt-1];
        }

        throw new SearchStrategyException('No preSearch result');
    }
}