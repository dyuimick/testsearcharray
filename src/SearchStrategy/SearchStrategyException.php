<?php

declare(strict_types=1);

namespace app\SearchStrategy;

use Exception;
class SearchStrategyException extends Exception
{
    public const MESSAGE = "Search Strategy Error";

    public function __construct($message = "", $code = 0)
    {
        $message = static::MESSAGE . ' : ' . $message;
        parent::__construct($message, $code);
    }
}