<?php

declare(strict_types=1);

namespace app\SearchStrategy;

use app\ArrayModel\SortedUniqueValueArray;

class SearchStrategyFactory
{
    public const LINER_STRATEGY = 'liner';
    public const BINARY_STRATEGY = 'binary';
    public const INTERPOLATION_STRATEGY = 'interpolation';

    public static function getStrategy(string $type): SearchStrategyInterface
    {
        switch ($type) {
            case self::LINER_STRATEGY:
                return new LineSearchStrategy();
            case self::BINARY_STRATEGY:
                return new BinarySearchStrategy();
            case self::INTERPOLATION_STRATEGY:
                return new InterpolationSearchStrategy();
            default:
                throw new SearchStrategyException('Unknown strategy');
        }
    }
}