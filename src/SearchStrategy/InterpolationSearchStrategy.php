<?php

declare(strict_types=1);

namespace app\SearchStrategy;

use app\ArrayModel\SortedUniqueValueArray;

class InterpolationSearchStrategy extends AbstractSearchStrategy
{
    public function findLowerThenTarget(int $target, SortedUniqueValueArray $numbers): int
    {
        if ($this->isPreSearchData($target, $numbers)){
            return $this->getPreSearchData($target, $numbers);
        }

        $low = 0;
        $high = count($numbers) - 1;
        $result = -1;

        while ($low <= $high && $target >= $numbers[$low] && $target <= $numbers[$high]) {
            if ($low >= $high) {
                break;
            }

            $pos = $low + (int)(($high - $low) / ($numbers[$high] - $numbers[$low]) * ($target - $numbers[$low]));

            if ($numbers[$pos] === $target) {
                return $pos - 1 >= 0 ? $numbers[$pos - 1] : -1;
            }

            if ($numbers[$pos] < $target) {
                $result = $numbers[$pos];

                $low = $pos + 1;
            } else {
                $high = $pos;
            }
        }

        return $result;
    }
}