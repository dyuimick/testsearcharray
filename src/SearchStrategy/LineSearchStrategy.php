<?php

declare(strict_types=1);

namespace app\SearchStrategy;

use app\ArrayModel\SortedUniqueValueArray;

class LineSearchStrategy extends AbstractSearchStrategy
{
    public function findLowerThenTarget(int $target, SortedUniqueValueArray $numbers): int
    {
        if ($this->isPreSearchData($target, $numbers)){
            return $this->getPreSearchData($target, $numbers);
        }

        $result = -1;

        foreach ($numbers as $number) {
            if ($number < $target) {
                $result = $number;
            } else {
                break;
            }
        }
        return $result;
    }
}