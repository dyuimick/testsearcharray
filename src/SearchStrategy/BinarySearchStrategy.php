<?php

declare(strict_types=1);

namespace app\SearchStrategy;

use app\ArrayModel\SortedUniqueValueArray;

class BinarySearchStrategy extends AbstractSearchStrategy
{
    public function findLowerThenTarget(int $target, SortedUniqueValueArray $numbers): int
    {
        if ($this->isPreSearchData($target, $numbers)){
            return $this->getPreSearchData($target, $numbers);
        }

        $start = 0;
        $end = count($numbers) - 1;
        $result = -1;

        while ($start <= $end) {
            $mid = $start + (int)(($end - $start) / 2);

            if ($numbers[$mid] < $target) {
                $result = $numbers[$mid];
                $start = $mid + 1;
            } else {
                $end = $mid - 1;
            }
        }

        return $result;
    }
}