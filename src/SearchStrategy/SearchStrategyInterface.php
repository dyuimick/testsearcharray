<?php

declare(strict_types=1);

namespace app\SearchStrategy;

use app\ArrayModel\SortedUniqueValueArray;

interface SearchStrategyInterface
{
    public function findLowerThenTarget(int $target, SortedUniqueValueArray $numbers): int;
}