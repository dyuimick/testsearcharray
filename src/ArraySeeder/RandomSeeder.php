<?php

declare(strict_types=1);

namespace app\ArraySeeder;

class RandomSeeder implements ArraySeederInterface
{
    private int $size;
    private int $min;
    private int $max;

    public function __construct(int $size, int $min, int $max)
    {
        $this->setSize($size);
        $this->setMinMaxValue($min, $max);
    }

    public function setSize(int $size): self
    {
        if ($size <= 0) {
            throw new RandomSeederException('Size should be greater than 0');
        }

        $this->size = $size;
        return $this;
    }

    public function setMinMaxValue(int $min, int $max): self
    {
        if ($min >= $max) {
            throw new RandomSeederException('Min should be less than Max');
        }

        $this->setMin($min)->setMax($max);
        return $this;
    }

    private function setMin(int $value): self
    {
        $this->min = $value;
        return $this;
    }

    private function setMax(int $value): self
    {
        $this->max = $value;
        return $this;
    }


    public function seed(): array
    {
        $randomNumbers = [];

        for ($i = 0; $i < $this->size; $i++) {
            $randomNumbers[] = random_int($this->min, $this->max);
        }

        return $randomNumbers;
    }
}