<?php

declare(strict_types=1);

namespace app\ArraySeeder;

interface ArraySeederInterface
{
    public function seed(): array;
}