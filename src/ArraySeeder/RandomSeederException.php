<?php

declare(strict_types=1);

namespace app\ArraySeeder;

use Exception;
class RandomSeederException extends Exception
{
    public const MESSAGE = "Seeder exception";

    public function __construct($message = "", $code = 0)
    {
        $message = static::MESSAGE . " : " . $message;
        parent::__construct($message, $code);
    }
}