<?php

declare(strict_types=1);

namespace app\Utils;

use app\SearchStrategy\SearchStrategyInterface;

class Utils
{
    public static function printResultForStrategy(int $number, SearchStrategyInterface $strategy): void
    {
        print(sprintf("\n %s  Result = %s\n", $strategy::class, $number));
    }
}