<?php

declare(strict_types=1);

namespace app\ArrayModel;

use ArrayAccess, Countable, IteratorAggregate, ArrayIterator;

class SortedUniqueValueArray implements ArrayAccess, Countable, IteratorAggregate
{
    private $data = [];

    public function __construct(array $data)
    {
        $uniqueData = array_unique($data);
        sort($uniqueData);
        $this->data = $uniqueData;
    }

    public function offsetExists($offset): bool
    {
        return isset($this->data[$offset]);
    }

    public function offsetGet($offset)
    {
        return $this->data[$offset];
    }

    public function offsetSet($offset, $value): void
    {
    }

    public function offsetUnset($offset): void
    {
    }

    public function count(): int
    {
        return count($this->data);
    }

    public function getIterator(): ArrayIterator
    {
        return new ArrayIterator($this->data);
    }
}