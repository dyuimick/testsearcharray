<?php

declare(strict_types=1);

use PHPUnit\Framework\TestCase;
use app\SearchStrategy\LineSearchStrategy;
use app\SearchStrategy\BinarySearchStrategy;
use app\SearchStrategy\InterpolationSearchStrategy;
use app\ArrayModel\SortedUniqueValueArray;

class SearchStrategyTest extends TestCase
{
    /**
     * @dataProvider dataProvider
     */
    public function testLinerSearchStrategy(array $inArray, int $target, int $expects)
    {
        $strategy = new LineSearchStrategy();
        $numbers = new SortedUniqueValueArray($inArray);

        $this->assertEquals($expects, $strategy->findLowerThenTarget($target, $numbers));
    }

    /**
     * @dataProvider dataProvider
     */
    public function testBinarySearchStrategy(array $inArray, int $target, int $expects )
    {
        $strategy = new BinarySearchStrategy();
        $numbers = new SortedUniqueValueArray($inArray);

        $this->assertEquals($expects, $strategy->findLowerThenTarget($target, $numbers));
    }

    /**
     * @dataProvider dataProvider
     */
    public function testInterpolationSearchStrategy(array $inArray, int $target, int $expects)
    {
        $strategy = new InterpolationSearchStrategy();
        $numbers = new SortedUniqueValueArray($inArray);

        $this->assertEquals($expects, $strategy->findLowerThenTarget($target, $numbers));
    }

    public static function dataProvider()
    {
        $fullArray = [1, 3, 5, 7, 9];
        $oneElementArray = [1];
        $emptyArray = [];

        return [
            'target above an element' => [$fullArray, 4, 3],
            'target below elements' => [$fullArray, 0, -1],
            'target above elements' => [$fullArray, 10, 9],
            'target equal element expecting lower' => [$fullArray, 9, 7],
            'array of one element' => [$oneElementArray, 9, -1],
            'empty array' => [$emptyArray, 7, -1],
        ];
    }
}
