<?php

declare(strict_types=1);

use PHPUnit\Framework\TestCase;
use app\ArraySeeder\RandomSeeder;
use app\ArraySeeder\RandomSeederException;

class RandomSeederTest extends TestCase
{
    public function testConstructValid()
    {
        $seeder = new RandomSeeder(5, 1, 10);
        $this->assertInstanceOf(RandomSeeder::class, $seeder);
    }

    public function testSetSizeThrowsExceptionInvalidSize()
    {
        $this->expectException(RandomSeederException::class);
        $this->expectExceptionMessage('Size should be greater than 0');

        new RandomSeeder(0, 1, 10);
    }

    public function testSetMinMaxValueThrowsExceptionInvalidRange()
    {
        $this->expectException(RandomSeederException::class);
        $this->expectExceptionMessage('Min should be less than Max');

        new RandomSeeder(5, 10, 1);
    }

    public function testSeedProducesArray()
    {
        $size = 10;
        $min = 1;
        $max = 100;
        $seeder = new RandomSeeder($size, $min, $max);
        $randomNumbers = $seeder->seed();

        $this->assertCount($size, $randomNumbers);

        foreach ($randomNumbers as $number) {
            $this->assertGreaterThanOrEqual($min, $number);
            $this->assertLessThanOrEqual($max, $number);
        }
    }
}
