# Search Strategies Collection

Implements search strategies to work with sorted arrays. 
The strategies include Linear Search, Binary Search, and Interpolation Search.

## Features

- **Linear Search Strategy**: A straightforward approach suitable for small datasets.
- **Binary Search Strategy**: An efficient method for large datasets with logarithmic time complexity.
- **Interpolation Search Strategy**: Offers improved performance over binary search by estimating the position of the target value.

## Getting Started

### Installation

Clone the repository to your local machine:

```bash
git clone git@gitlab.com:dyuimick/testsearcharray.git
cd testsearcharray
```

### Run Example

```bash
php index.php
```

### Run Unit Tests

```bash
./vendor/bin/phpunit tests
```