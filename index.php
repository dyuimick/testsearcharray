<?php
require 'vendor/autoload.php';

use app\ArrayModel\SortedUniqueValueArray;
use app\ArraySeeder\RandomSeeder;
use app\SearchStrategy\LineSearchStrategy;
use app\SearchStrategy\BinarySearchStrategy;
use app\SearchStrategy\InterpolationSearchStrategy;
use app\Utils\Utils;

$numbers = new SortedUniqueValueArray([ 3, 4, 6, 9, 10, 12, 14, 15, 17, 19, 21 ]);
#$numbers = new SortedUniqueValueArray((new RandomSeeder(10, 1,30))->seed());

$strategy = new LineSearchStrategy();

Utils::printResultForStrategy($strategy->findLowerThenTarget(11, $numbers), $strategy);

$strategy = new BinarySearchStrategy();
Utils::printResultForStrategy($strategy->findLowerThenTarget(14, $numbers), $strategy);

$strategy = new InterpolationSearchStrategy();
Utils::printResultForStrategy($strategy->findLowerThenTarget(10, $numbers), $strategy);
